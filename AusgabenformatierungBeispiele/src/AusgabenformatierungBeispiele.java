public class AusgabenformatierungBeispiele {

    public static void main(String[] args) {

        System.out.printf("|%s|%n","123456789");
        System.out.printf("|%20s|\n","123456789");
        System.out.printf("|%-20s|\n","123456789");
        System.out.printf("|%-20.3s|\n","123456789");




        // Ganzzahl
/*
        System.out.printf("|%d|%n",123456789);
        System.out.printf("|%20d|%n",123456789);
        System.out.printf("|%-20d|%n",123456789);
        System.out.printf("|%-20d|%n",-123456789);
        System.out.printf("|%-20d|%n",123456789);
        System.out.printf("|%+020d|%n",123456789);


 */

        // Kommazahl
/*
        System.out.printf("|%f|%n",123456.789);
        System.out.printf("|%.2f|%n",123456.789);
        System.out.printf("|%20.2f|%n",123456.789);
        System.out.printf("|%-20.2f|%n",123456.789);
        System.out.printf("|%020.2f|%n",123456.789);
        System.out.printf("|%+020.2f|%n",123456.789);



 */







    }

}
