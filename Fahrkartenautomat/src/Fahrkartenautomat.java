import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {

        int count = 0;

        do {
            double zuZahlen = fahrkartenbestellungErfassen();
            double rückgabebetrag = fahrkartenBezahlen(zuZahlen);
            fahrkartenAusgeben();
            rückgeldAusgeben(rückgabebetrag);

            System.out.println();
        } while (count < 1);
    }

    public static double fahrkartenbestellungErfassen() {

        System.out.println("\nFahrkartenbestellvorgang");
        for (int i = 0; i < 25; i++) {
            System.out.print("=");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n");

        Scanner tastatur = new Scanner(System.in);

        double[] fahrkartenpreise = new double[10];

        fahrkartenpreise[0] = 2.90;
        fahrkartenpreise[1] = 3.30;
        fahrkartenpreise[2] = 3.60;
        fahrkartenpreise[3] = 1.90;
        fahrkartenpreise[4] = 8.60;
        fahrkartenpreise[5] = 9.00;
        fahrkartenpreise[6] = 9.60;
        fahrkartenpreise[7] = 23.50;
        fahrkartenpreise[8] = 24.30;
        fahrkartenpreise[9] = 24.90;

        String[] fahrkartenbezeichnung = new String[10];
        fahrkartenbezeichnung[0] = "Einzelfahrschein Berlin AB";
        fahrkartenbezeichnung[1] = "Einzahlfahrschein Berlin BC";
        fahrkartenbezeichnung[2] = "Einzelfahrschein Berlin ABC";
        fahrkartenbezeichnung[3] = "Kurzstrecke";
        fahrkartenbezeichnung[4] = "Tageskarte Berlin AB";
        fahrkartenbezeichnung[5] = "Tageskarte Berlin BC";
        fahrkartenbezeichnung[6] = "Tageskarte Berlin ABC";
        fahrkartenbezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
        fahrkartenbezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
        fahrkartenbezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";

        int anzahlTickets;
        int ticketentscheidung;
        double gesamtpreis = 0;

        do {

            double schleifenPreis = 0;

            System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:");
            for (int i = 0; i < fahrkartenpreise.length; i++) {

                String lückenfüller1 = "";
                String lückenfüller2 = "";

                //für die schönere Ausgabe in der Konsole
                if (i >= 9) {
                    lückenfüller1 += "    ";
                } else {
                    lückenfüller1 += "     ";
                }
                if (i == 0) {
                    System.out.println("0" + lückenfüller1 + "Bezahlen");
                }
                for (int j = 0; j <= 40 - fahrkartenbezeichnung[i].length(); j++) {
                    lückenfüller2 += ' ';
                }
                System.out.println(i+1 + lückenfüller1 +  fahrkartenbezeichnung[i] + lückenfüller2 + fahrkartenpreise[i] + "€");
            }

            System.out.print("Ihre Wahl: ");
            ticketentscheidung = tastatur.nextInt();

            if (ticketentscheidung == 0) {
                break;
            }
            schleifenPreis += fahrkartenpreise[ticketentscheidung-1];

            System.out.print("Anzahl der Tickets (mind.1, max.10): ");
            anzahlTickets = tastatur.nextInt();

            if (anzahlTickets >= 1 && anzahlTickets <= 10) {

            } else if (anzahlTickets < 1 || anzahlTickets > 10) {
                anzahlTickets = 1;
                System.out.println("Ihre Ticketeingabe ist ungültig. Die Ticketanzahl wurde automatisch auf 1 gesetzt.");
            }

            schleifenPreis = schleifenPreis * anzahlTickets;

            gesamtpreis += schleifenPreis;

        } while (ticketentscheidung != 0);

        return gesamtpreis;

    }

    public static double fahrkartenBezahlen(double zuZahlen) {

        Scanner tastatur = new Scanner(System.in);
        System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

        double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
        double rückgabeBetrag;

        while (eingezahlterGesamtbetrag < zuZahlen) {
            System.out.print("Noch zu zahlen: ");
            //der zu zahlende Betrag wird mit der Anzahl der Tickets multipliziert
            System.out.printf("%.2f", (zuZahlen - eingezahlterGesamtbetrag));
            System.out.printf("%s\n", " Euro");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        rückgabeBetrag = eingezahlterGesamtbetrag - zuZahlen;

        return rückgabeBetrag;
    }

    public static void fahrkartenAusgeben() {

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void rückgeldAusgeben(double rückgabebetrag) {

        if (rückgabebetrag >= 0.0) {

            System.out.print("Der Rückgabebetrag in Höhe von ");
            System.out.printf("%.2f", rückgabebetrag);
            System.out.printf("%s\n", " Euro");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while (rückgabebetrag >= 0.049)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }


        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
    }

    public static void warte() {

        Scanner tastatur = new Scanner(System.in);
        System.out.println("Geben Sie die Zeit an, die das Programm warten soll. (Millisekunden)");
        //double eingabeMilli = tastatur.nextDouble();
        long milli = 250;

    }

}