public class BenutzerTest {

    public static void main(String[] args) {


        //Anlegen von Benutzer Objekten
        Benutzer benutzerEins = new Benutzer("Marko");
        Benutzer benutzerZwei = new Benutzer("Anna");


        //benutzerEins set
        benutzerEins.setPasswort("aHfIW39!jd9");
        benutzerEins.setEmailAdresse("Marko@gmail.com");

        //benutzerEins get
        System.out.println("Name: " + benutzerEins.getName());
        System.out.println("Passwort:" + benutzerEins.getPasswort());
        System.out.println("E-Mail Adresse: " + benutzerEins.getEmailAdresse());

        //benutzerEins zusaetzliche Methoden
        benutzerEins.gehen();
        benutzerEins.rennen();

        //Leerzeile zwischen Benutzerausgaben
        System.out.println();

        //benutzerZwei set
        benutzerZwei.setPasswort("ihchHU99e!huch;");
        benutzerZwei.setEmailAdresse("Anna@outlook.com");

        //benutzerZwei get
        System.out.println("Name: " + benutzerZwei.getName());
        System.out.println("Passwort:" + benutzerZwei.getPasswort());
        System.out.println("E-Mail Adresse: " + benutzerZwei.getEmailAdresse());

        //benutzerZwei zusaetzliche Methoden
        benutzerZwei.gehen();
        benutzerZwei.rennen();

    }
}
