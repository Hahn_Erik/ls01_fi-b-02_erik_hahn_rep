public class Benutzer {

    private String name;
    private String passwort;
    private String emailAdresse;


    public Benutzer(String name) {
        this.name = name;
    }

    public void gehen() {
        System.out.println(this.getName() + " geht jetzt.");
    }

    public void rennen() {
        System.out.println(this.getName() + " rennt jetzt.");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setEmailAdresse(String emailAdresse) {
        this.emailAdresse = emailAdresse;
    }

    public String getEmailAdresse() {
        return emailAdresse;
    }

}
